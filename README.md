# Team-Fortress-2-Config

## Features
* Null cancellation movement
* Engineer quick-build/destroy
* Spy quick disguise
* Spy quick sap
* Soldier rocketjump bind
* Aliases to call for Ubercharge:

## Installing
Simply download the zip file and add the config files into "Team Fortress 2/tf/custom/[Folder name]/cfg" 

## Usage
### Spy binds
* Shift+[1-9] - disguise as enemy team
* Alt+[1-9] - disguise as friendly team
* Shift+x - disguise as the next enemy class
* Shift+z - disguise as the previous enemy class
* Alt+x - disguise as the next friendly class
* Alt+z - disguise as the previous friendly class
* Shift+MOUSE1 - Sap and change to primary
* Shift+MOUSE2 - Sap and change to melee

### Engineer binds
All build commands will destroy any previously built building of that type
* Shift+MOUSE1 - build a a sentry
* Mouse scroll wheel will only cycle through 1-3 and will skip the build and destroy PDA

### Soldier binds
* Shift click for rocketjump bind (+jump; +duck; +attack)
* Releasing shift will turn off the rocketjump bind activate "-jump; -duck;" commands

### All
* e calls for medic
* Shift+e calls for Ubercharge